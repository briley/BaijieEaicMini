<div>

    <div class="modal ${active}">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title" u-v="dialog.title"></p>
                <button class="delete" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <div u-v="dialog.text"></div>
            </section>
            <footer class="modal-card-foot">
                <button class="button is-success" u-click="onOK">确定</button>
                <button class="button" u-click="onCancel">取消</button>
            </footer>
        </div>
    </div>




</div>