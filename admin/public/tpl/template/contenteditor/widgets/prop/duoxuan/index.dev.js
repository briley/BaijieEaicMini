$.app({
    data:{
        entity:null
    },

    onChange:function (e) {
        var me = this;
        me.setData({
            action:{
                'entityPropChange' : me.data.entity
            }
        })
    },
	onAddChoice:function (e) {
		var me = this;
		var entity = me.data.entity;
		if( entity && entity.value && $.isArray(entity.value.choices)){
			entity.value.choices.push("新增选项");
			me.setData({
				action:{
					'entityPropChange' : me.data.entity
				}
			});
		}

	},
	onChoiceChange:function (e) {
		var me = this;
		me.setData({
			action:{
				'entityPropChange' : me.data.entity
			}
		})
	},
	onRemoveChoice:function (e) {
		var me = this;
		var index = $.toInt(e.data.index);
		me.data.entity.value.choices.splice(index,1);
		me.setData({
			action:{
				'entityPropChange' : me.data.entity
			}
		})
	}
});