<div>
    <div u-v="entity.value.title" class="bm-m"></div>
    <div>
        <div u-each="numbers" class="dafen-number" u-index="index">
            <i class="{| fa fa-star-o dafen-star ${overindex>=index ?'active':''} |}"
               data-index="${index}"
               u-mouseout="onMouseLeave"
               u-mouseover="onMouseOver"></i>
        </div>
    </div>

</div>