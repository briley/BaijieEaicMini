$.app({
    data:{
        selday:null,//入参：选中的日期

        today:new Date(),//今天
        selday_str:"",
        rows:[],
        titles:["日","一","二","三","四","五","六"],
        showday_text:'',
        showday:new Date(),//当前所在页日期
        pop_top:0,//弹框位置
        pop_pos: '-9999px'//弹框位置
    },
    onLoad:function () {
        var me = this;

        me.goPage(me.data.showday);
    },
    onPop:function (e) {
        var me = this;
        var dom = e.src;
        var h = 240;
        var pop_top=34;
        var pop_pos=0;
        if( dom.getBoundingClientRect().top+h > window.innerHeight ) pop_top=-h;
        me.setData({
            pop_pos: ''+pop_pos+'px',
            pop_top: ''+pop_top+'px'
        })
    },
    onHide:function (e) {
        var me = this;
        var pop_pos= -9999;
        me.setData({
            pop_pos: ''+pop_pos+'px'
        })
    },
    getShowDayStr:function () {
        var me = this;
        var day = me.data.showday;
        var y = day.getFullYear();
        var m = day.getMonth()+1;
        return day.getFullYear()+"-"+(m <10 ? '0':'')+m;
    },
    getSelDay_str:function () {
        var me = this;
        var day = new Date(me.getSelDay());
        var y = day.getFullYear();
        var m = day.getMonth()+1;
        var d = day.getDate();
        return me.formatDate(day);
    },
    //得到选中的日期，默认为“今天”
    getSelDay:function () {
        var me = this;
        if( !me.data.selday ) me.data.selday = me.data.today;
        return new Date(me.data.selday);
    },
    //根据日期转换为字符串,自动填充0
    formatDate:function (day) {
        var y = day.getFullYear();
        var m = day.getMonth()+1;
        var d = day.getDate();
        return y+"-"+(m <10 ? '0':'')+m+"-"+(d<10?'0':'')+d;
    },
    //获取传入时间当前的月份有多少天
    getMonthDays: function(date) {
        date.setDate(1);
        var firstDay = date.getDay();
        var monthStart = date.getTime();
        date.setMonth(date.getMonth() + 1);
        var monthEnd = date.getTime();
        var days = Math.ceil((monthEnd - monthStart) / (24 * 60 * 60 * 1000));
        date.setMonth(date.getMonth() - 1);
        return {
            days: days,
            firstDay: firstDay
        };

    },
    //给定日期是否为今天
    isToday:function (date) {
        var td = new Date();
        return td.getFullYear() === date.getFullYear() && td.getMonth() === date.getMonth() && td.getDate() === date.getDate();
    },
    //翻到指定页
    goPage:function (date) {
        var me = this;

        var dinfo = me.getMonthDays(date);
        var rowcount = Math.ceil((dinfo.days+dinfo.firstDay)/7);


        var rows=[];
        var row=[];

        //first day
        var d = new Date(date);
        d.setDate(1);
        var first = d;
        d = new Date(date);
        d.setMonth(d.getMonth()+1);
        d.setDate(0);
        var last = d;

        var cellcount = rowcount * 7;
        var tmpDate = new Date(date);
        tmpDate.setDate(1-dinfo.firstDay);
        for(var i=0;i<cellcount;i++){
            var obj = {};
            if( i % 7 === 0 ) {
                row = [];
                rows.push(row);
            }

            var dx = new Date(tmpDate);
            dx.setDate( dx.getDate() + i);

            obj.date = dx;//单元元对应的日期
            obj.isPrev = (i < dinfo.firstDay ? 'prev' : '');
            obj.isNext = (i >= dinfo.firstDay+dinfo.days ? 'next':'');
            obj.isToday = (me.isToday(dx) ? 'today' : '');
            obj.isSel = (me.isSel(dx) ? 'active':'');
            obj.isCell= (i >= dinfo.firstDay && i < dinfo.firstDay+dinfo.days ? 'cell':'');

            row.push(obj);
        }

        me.setData({
            rows:rows,
            showday_text:me.getShowDayStr(),
            selday_str:me.getSelDay_str()
        });//更新界面
    },
    isSel:function (d) {
        var me = this;
        var f = me.getSelDay();

        return d.getFullYear() === f.getFullYear() &&
            d.getMonth() === f.getMonth() &&
            d.getDate() === f.getDate();
    },
    onPrevMonth:function (e) {
        var me = this;
        var d = me.data.showday;
        d.setMonth(d.getMonth()-1);
        me.goPage(d);
    },
    onNextMonth:function (e) {
        var me = this;
        var d = me.data.showday;
        d.setMonth(d.getMonth()+1);
        me.goPage(d);
    },
    onPrevYear:function (e) {
        var me = this;
        var d = me.data.showday;
        d.setFullYear(d.getFullYear()-1);
        me.goPage(d);
    },
    onNextYear:function (e) {
        var me = this;
        var d = me.data.showday;
        d.setFullYear(d.getFullYear()+1);
        me.goPage(d);
    },
    onSel:function (e) {
        var me = this;
        var row = $.toInt( e.data.row );
        var col = $.toInt( e.data.col );
        var sel = me.data.rows[row][col];
        //$.log('sel-->'+sel.date);
        me.setData({
            selday : me.formatDate(sel.date),
            pop_pos: '-9999em'
        });
        me.goPage(sel.date);
        //$.log('2 sel-->'+sel.date);
    }


});