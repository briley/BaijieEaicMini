<div>
    <nav class="pagination" role="navigation" aria-label="pagination" style="margin:auto -0.25rem">
        <a class="pagination-previous is-hidden">←上一页</a>
        <a class="pagination-next is-hidden">下一页→</a>
        <ul class="pagination-list">
            <li
                title="首页" class=''
                u-click="go_page" pageindex="1">
                <a class="pagination-link" aria-label="1">←</a>
            </li>
            <li u-each="pagerange(pageindex,pagecount)"
                 u-item="item"
                 title="第${item}页" class=''
                 u-click="go_page" pageindex="${item}">
                <a class="pagination-link ${item==pageindex ? 'is-current':''}" aria-label="${item}">${item}</a>
            </li>
            <li
                    title="末页" class=''
                    u-click="go_page" pageindex="${pagecount}">
                <a class="pagination-link" aria-label="1">→</a>
            </li>
        </ul>
    </nav>


</div>