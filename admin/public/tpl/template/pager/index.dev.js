eui.app({
    data:{
        //page_index data_count page_count
        pagerange : function ($pageindex, $pagecount) {
            if ($pagecount < 1) return [];
            var $start = ($pageindex > 4 ? $pageindex - 4 : 1);
            var $end = ($pageindex + 4 <= $pagecount ? $pageindex + 4 : $pagecount);
            var $arr = [];
            if ($end >= $start) {
                for (var $index = $start; $index <= $end; $index++)
                    $arr.push($index);
            }
            return $arr;
        },
        pageindex:1,
        pagecount:0,
        datacount:0
    },

    go_page:function (e) {

        this.setData({
            pageindex : eui.toInt( eui.dom(e.src).get("pageindex"))
        })
    }

});