(function () {
	return [
		{
			key:"slidebox",
			icon:"/tpl/admin/ui/controls/slidebox/images/slidebox.png",
			isNormal:true,
			style:"",
			pageUrl:"/index;/tabpage",
			hint:"轮播",
			onCreate:function (entity) {
				entity.value.images=[];

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				entity.value.size.height.type=4;
				entity.value.size.height.value=120;
			}
		},
		{
			key:"productlist",
			icon:"/tpl/admin/ui/controls/productlist/images/productlist.png",
			isNormal:true,
			style:"",
			pageUrl:"/index;/tabpage",
			hint:"产品列表",
			onCreate:function (entity) {
				entity.value.numperline=2;//每行显示产品个数
				entity.value.datasource="";//数据源provider

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				//entity.value.size.height.type=4;
				//entity.value.size.height.value=120;
			}
		},
		{
			key:"metalarea",
			icon:"/tpl/admin/ui/controls/metalarea/images/metalarea.png",
			isNormal:true,
			style:"",
			pageUrl:"/index;/tabpage",
			hint:"金刚区",
			onCreate:function (entity) {
				entity.value.linebtncount=4;//每行按钮个数
				entity.value.entries=[];//功能入口 [{icon:"",title:"",type:0,url:""},...]
				//type=0 打开预定义页面
				//type=1 打开指定页面
				//type=2 打开指定网址
				//

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				entity.value.size.height.type=4;
				entity.value.size.height.value=120;
			}
		},
		{
			key:"scrollview",
			icon:"/tpl/admin/ui/controls/scrollview/images/scrollview.png",
			isNormal:true,
			isContainer:true,
			style:"",
			pageUrl:"/index;/tabpage",
			hint:"滚动容器",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				//entity.value.size.height.type=4;
				//entity.value.size.height.value=120;
			}
		},
		{
			key:"tabbar",
			icon:"/tpl/admin/ui/controls/tabbar/images/tabbar.png",
			isNormal:true,
			isContainer:true,
			style:"",
			pageUrl:"/index",
			hint:"页卡栏",
			onCreate:function (entity) {
				entity.value.tabs=[];//标签页 [{icon:"",title:"",iconSelected:""},...]
				entity.value.fontColor="";
				entity.value.fontColorSelected="";

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				//entity.value.size.height.type=4;
				//entity.value.size.height.value=120;
			}
		},
		{
			key:"productcat",
			icon:"/tpl/admin/ui/controls/productcat/images/productcat.png",
			isNormal:true,
			isContainer:false,
			style:"",
			pageUrl:"/tabpage",
			hint:"产品分类",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				entity.value.size.height.type=4;
				entity.value.size.height.value=120;
			}
		},
		{
			key:"order",
			icon:"/tpl/admin/ui/controls/order/images/order.png",
			isNormal:true,
			isContainer:false,
			style:"",
			pageUrl:"/tabpage",
			hint:"订单",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				entity.value.size.height.type=4;
				entity.value.size.height.value=120;
			}
		},
		{
			key:"listgroup",
			icon:"/tpl/admin/ui/controls/listgroup/images/listgroup.png",
			isNormal:true,
			isContainer:true,
			style:"",
			pageUrl:"/tabpage",
			hint:"列表组",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				//entity.value.size.height.type=4;
				//entity.value.size.height.value=120;
			}
		},
		{
			key:"listitem",
			icon:"/tpl/admin/ui/controls/listitem/images/listitem.png",
			isNormal:true,
			isContainer:false,
			style:"",
			pageUrl:"/tabpage",
			hint:"列表项",
			onCreate:function (entity) {
				entity.value.title="列表项";
				entity.value.icon="";
				entity.value.type=0;
				entity.value.pageid=0;
				entity.value.url="";
				entity.value.params="";

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				//entity.value.size.height.type=4;
				//entity.value.size.height.value=120;
			}
		},
		{
			key:"loginuser",
			icon:"/tpl/admin/ui/controls/loginuser/images/loginuser.png",
			isNormal:true,
			isContainer:false,
			style:"",
			pageUrl:"/tabpage",
			hint:"登录用户信息",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				entity.value.size.height.type=4;
				entity.value.size.height.value=120;
			}
		},
		{
			key:"orderentry",
			icon:"/tpl/admin/ui/controls/orderentry/images/orderentry.png",
			isNormal:true,
			isContainer:false,
			style:"",
			pageUrl:"/tabpage",
			hint:"订单入口",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				entity.value.size.height.type=4;
				entity.value.size.height.value=45;
			}
		},
	]
})();