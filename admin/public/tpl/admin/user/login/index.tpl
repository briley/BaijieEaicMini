<!DOCTYPE html>

<!--[if IE 8]>
<html lang="zh-CN" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="zh-CN" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-CN">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
	<meta charset="utf-8"/>
	<title>{{site_title}}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="{{site_desc}}" name="description"/>
	<meta content="{{site_auth}}" name="author"/>

	<link href="/ebfui/font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="/tpl/theme/default/css/eui.css" rel="stylesheet" type="text/css"/>


	<script src="/ebfui/js/eui.min.js" type="text/javascript"></script>

	<!--[if lt IE 10]>
	<script src="/ebfui/vendor/history.min.js" type="text/javascript"></script>
	<script src="/ebfui/vendor/promise.min.js" type="text/javascript"></script>
	<![endif]-->
	<!-- @@==cssfile==@@ -->
	<!-- @@==csscode==@@ -->
</head>
<!-- END HEAD -->

<body >

	<div id="frame" >

	</div>
<!-- END PAGE LEVEL PLUGINS -->



<script type="text/javascript">
    eui.setConfig({
        'debug'		:	eui.toInt("{{isdebug}}"),
        'siteurl'	:	"{{site_url}}",
        'sitename' : "{{site_name}}"
    });

    $.run("#frame","/admin/user/login/frame");

</script>


<!-- @@==jsfile==@@ -->
<!-- @@==jscode==@@ -->

</body>


</html>




