<!DOCTYPE html>

<!--[if IE 8]>
<html lang="zh-CN" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="zh-CN" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-CN">
<!--<![endif]-->

<head>
    <meta charset="utf-8"/>
    <title>{{site_title}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
    <meta content="{{site_desc}}" name="description"/>
    <meta content="{{site_auth}}" name="author"/>


    <link href="/ebfui/font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/tpl/theme/default/css/eui.css" rel="stylesheet" type="text/css"/>

    <script src="https://res.wx.qq.com/open/js/jweixin-1.3.2.js" type="text/javascript"></script>


    <script src="/ebfui/js/eui.min.js" type="text/javascript"></script>
 

    <!--[if lt IE 10]>
    <script src="/ebfui/vendor/history.min.js" type="text/javascript"></script>
    <script src="/ebfui/vendor/promise.min.js" type="text/javascript"></script>
    <![endif]-->

    <script src="/ebfui/vendor/clipboard.min.js" type="text/javascript"></script>


    <!-- @@==cssfile==@@ -->
    <!-- @@==csscode==@@ -->
</head>
<body >
<div id="frame">
</div>
<script type="text/javascript">
    eui.setConfig({
        'debug'		:	eui.toInt("{{isdebug}}"),
        'siteurl'	:	"{{site_url}}",
        'bizroot'   :   '/bizservice',
    });

    var ue = eui.getConfig().userexception;
    ue["1024"] = function(err){
        //未登录
        $.redirect("{{site_url}}/user/login");
    };

    $.setWxTitle=function (title) {
		try{
			document.title=title;
			var iframe = document.createElement('iframe');
			iframe.src = '/ebfui/js/eui.min.js';
			iframe.style.display = 'none';
			(document.body||document.documentElement).appendChild(iframe);
			iframe.onload = function() {
				setTimeout(function() {
					iframe.remove()
				}, 0);
			}
			//wx.setNavigationBarTitle({title: title})
        }catch(e){

        }

	};

    $.run("#frame","/console/frame/index");

</script>
<!-- @@==jsfile==@@ -->
<!-- @@==jscode==@@ -->
</body>

</html>




