$.app({
	data: {
		file: null,
		url:"",
		hint:"点击或拖放文件到此处",
		config: {}
	},
	on:{
		"file":function () {
			var me = this;
			if( $.isString(me.data.file) )
				me.setData({url:me.data.file});
        }
	},
	updateFile: function (file) {
		var me = this;


	},
	onFileChange: function (e) {
		var me = this;

		var file = e.src.files[0];

	
		var fnFindImage = function () {
			var parent = e.src.parentNode.parentNode.parentNode;
			var nodecount = parent.childNodes.length;
			for (var i = 0; i < nodecount; i++) {
				var node = parent.childNodes[i];
				if (node.nodeType === 1 && node.tagName.toLowerCase() === 'img') {
					return node;
					//break;
				}
			}
			return null;
		};
		var image = fnFindImage();


		// 压缩图片需要的一些元素和对象
		var targetWidth, targetHeight;

		var img = new Image();
		img.onload = function () {
			// 图片原始尺寸
			var originWidth = this.width;
			var originHeight = this.height;
			// 最大尺寸限制
			var maxWidth = 600,
				maxHeight = 500;
			// 目标尺寸
			targetWidth = originWidth,
				targetHeight = originHeight;
			// 图片尺寸超过400x400的限制
			if (originWidth > maxWidth || originHeight > maxHeight) {
				if (originWidth / originHeight > maxWidth / maxHeight) {
					// 更宽，按照宽度限定尺寸
					targetWidth = maxWidth;
					targetHeight = Math.round(targetWidth * (originHeight / originWidth));
				} else {
					targetHeight = maxHeight;
					targetWidth = Math.round(targetHeight * (originWidth / originHeight));
				}
			}
			image.width = targetWidth;
			image.height = targetHeight
		};

		var reader = new FileReader();
		reader.onload = function (e) {
			img.src = e.target.result;
			image.src = e.target.result
		};


		// 选择的文件是图片
		if (file.type.indexOf("image") === 0) {
			me.setData({ file: file });
			reader.readAsDataURL(file);
		}
	}
});