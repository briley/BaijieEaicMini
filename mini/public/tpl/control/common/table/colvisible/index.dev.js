$.app({
    data:{
        cols:{}//{ name:{text:"名称",visible:true},...}
    },
    onToggle:function (e) {
        var me = this;
        var index = e.data.index;
        var col = me.data.cols[index];
        col.visible = !col.visible;
        me.setData({
            cols:me.data.cols
        })
    }
});