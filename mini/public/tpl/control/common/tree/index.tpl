<div>
    <div u-each="nodes" u-item="node" u-index="nodeindex">

            <div draggable="true"
                 data-index="${nodeindex}"
                 u-dragstart="onDragStart"
                 u-dragover="onDragOver"
                 u-dragleave="onDragLeave"
                 u-drop="onDrop"
                 u-click="onTreeClick"
                 class="level ${node.dragover} tree-row ${ !activeNode || activeNode.key != node.key ? '':'active' } ${node.expand}">

                <div class="level-left">
                    <div u-if="node.children" class="level-item">
                        <i class="fa fa-angle-${node.expand ? 'down' : 'right'}" u-click="onExpand" data-index="${ nodeindex }"></i>
                    </div>
                    <div u-else="" class="level-item">
                        <i class="fa fa-angle-right" style="color:rgba(255,255,255,0)" ></i>
                    </div>
                    <div class="level-item tree-row-title" data-index="${ nodeindex }">
                        <img u-if="node.icon" src="${node.icon}"/>
                        ${node.title}
                    </div>
                </div>
                <div class="level-right">
                    <div u-if="node.menu" class="level-item text-right">
                        <nav class="treepop" >
                            <a u-click="onPop" data-index="${nodeindex}">
                                <i class="fa fa-ellipsis-v"></i>
                            </a>
                            <div class="tree-menu-wrap ${node.menuvisible ? '' : 'tree-menu-hide'}"
                                 data-index="${nodeindex}"
                                 style="position:fixed;top:${node.menutop || 0}px;${node.menuvisible?'left:'+node.menuleft+'px':''}"
                                 u-outside-click="onHide">
                                <div u-view="${node.menu}"
                                     u-share="{'node':'nodes[${nodeindex}]',
                                     'menuvisible':'nodes[${nodeindex}].menuvisible',
                                     'action.addNode':'action.addNode',
                                     'action.editNode':'action.editNode',
                                     'action.removeNode':'action.removeNode',
                                     }">

                                </div>

                            </div>
                        </nav>
                    </div>
                </div>


            </div>

            <div u-if="node.children" class="lp-n ${node.expand ? '' : 'is-hidden'}" >

                <div
                     u-view="/control/common/tree/index"
                     u-share="{
                     'activeNode':'activeNode',
                     'nodes':'nodes[${nodeindex}].children',
                     'menu':'menu',
                     'action.moveNode':'action.moveNode',
                     'action.moveContent':'action.moveContent'}">

                </div>

            </div>

    </div>
</div>