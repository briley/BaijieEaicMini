$.app({
    data:{
        /*
        {
            title:"",
            key:0,
            icon:"",
            children:[],
            expand:'expand',
            menu:"",//menu对应的u-view
            menuvisible:false
        }
        * */
        nodes:[], //title key icon children expand menuleft menutop
        menu:"",//缺省的弹出菜单地址
        activeNode:null//当前选中节点

    },
    on:{
        "action":{
            'addNode':function (node) {
                var me = this;
                $.log("addNode:"+$.toJson(node));
                var pid = node.pkey;
                var pnode = me.findNode(null,pid);
                $.log("found pnode:"+(pnode ? pnode.key : 'none'));
                var children = pnode ? pnode.children : me.data.nodes;
                var newNode = node;
                newNode.menu = me.data.menu;
                newNode.children=[];
                children.push(newNode);
                me.setData({activeNode:newNode});

            },
            'editNode':function (node) {
                var me = this;
                var thisNode = me.findNode(null,node.key);
                if( thisNode ){
                    thisNode.title = node.title;
                    me.setData();
                }
            },
            'removeNode':function (node) {
                var me = this;
                me.removeNode(node);
                var activeNode = me.data.activeNode;
                if( me.data.activeNode && me.data.activeNode.key == node.key ){
                    activeNode = null;
                }
                me.setData({
                    activeNode:activeNode
                });

            }

        }
    },
    onTreeClick:function (e) {
        var me = this;
        var node = me.data.nodes[ $.toInt(e.data.index) ];

        $.log("onTreeClick...["+node.key+"]");
        me.setData({ activeNode: node });

    },
    onExpand:function (e) {
        var me = this;
        var node = me.data.nodes[ $.toInt(e.data.index) ];
        if( node.children.length )
            node.expand = node.expand ? '' : 'expand';

        me.setData({
            //nodes: me.data.nodes
        });

    },
    onPop:function (e) {
        var me = this;
        var dom = e.src;

        $.log("onpop++++++ ****");
        var node = me.data.nodes[$.toInt(e.data.index)];

        var r = dom.getBoundingClientRect();
        var pop_top= r.y+34;//dom.offsetTop+34-dom.scrollTop;
        var pop_left= r.x;//dom.offsetLeft-30;
        //if( dom.getBoundingClientRect().top+h > window.innerHeight ) pop_top=-h;
        //绝对位置 var pos = dom.getBoundingClientRect();
        node.menuvisible=true;
        node.menuleft=pop_left;
        node.menutop = pop_top;
        me.setData({
            //nodes:me.data.nodes ,
            action:{
                hidemenu:{
                    all:true
                },
                showmenu:{
                    obj:node
                }
            }
        });
        return e.stop();
    },
    onHide:function (e) {
        var me = this;

        var node = me.data.nodes[$.toInt(e.data.index)];
        if( node.menuvisible ){
            node.menuvisible=false;

            me.setData({  });
        }
        
    },
    onDragStart:function (e) {
        var me = this;
        $.log("startDrag======");
        e.dataTransfer.setData("Text",me.data.nodes[$.toInt(e.data.index)].key);
    },
    onDragOver:function (e) {
        var me = this;
        //$.draginfo = e;
        //$.log("onDragOver=====");
        var node = me.data.nodes[$.toInt(e.data.index)];
        node.dragover = "dragover";
        me.setData();
        return e.stop();
    },
    onDragLeave:function (e) {
        var me = this;
        //$.log("onDragOver=====");
        var node = me.data.nodes[$.toInt(e.data.index)];
        node.dragover = "";
        me.setData();
        return e.stop();
    },
    onDrop:function (e) {
        var me = this;
        $.log("onDrop...");
        e.stop();

        var toNode = me.data.nodes[$.toInt(e.data.index)];
        toNode.dragover = "";

        var content = e.dataTransfer.getData("Content");
        if( content ){
            $.dragObject = content;
            $.log("拖放内容:"+$.toJson(content));
            var to = toNode.key;
            me.setData({
                action:{
                    moveContent:{from:content,to:to}
                }
            });
        }
        else{
            var fromKey=e.dataTransfer.getData("Text");

            var toKey = toNode.key;
            if( toKey !== fromKey ){
                $.log("开始移动节点...");
                me.setData({
                    action:{
                        moveNode:{from:fromKey,to:toKey}
                    }
                });
            }
        }


        return true;
    },
    findNode:function(parent,key){
        var me = this;
        var cs = (parent ? parent.children : me.data.nodes);
        var ret = null;
        $.each(cs,function(c){
            if( c.key == key ){
                ret = c;
                return true;
            }
            ret = me.findNode(c,key);
            if( ret ) return true;
        });
        return ret;
    },
    removeNode:function (node) {
        this._removeNode(null,node);
    },
    _removeNode:function (p,node) {
        var me = this;
        var cs = (p ? p.children : me.data.nodes);
        var ret = $.indexOfArray(node,cs,function (a,b) {
            return a.key === b.key;
        });
        if( ret >= 0 ){
            cs.splice(ret,1);
        }
        else{
            $.each(cs,function(c){
                ret = me._removeNode(c,node);
                if( ret >= 0 ) return true;
            });
        }

        return ret;
    }
});