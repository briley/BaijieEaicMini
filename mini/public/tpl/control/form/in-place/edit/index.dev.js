$.app({
    data:{
        value:"",
        key:"",
        valuetmp:"",
        defvalue:"未设置",
        inedit:false
    },
    onLoad:function () {
        var me = this;
        if( !me.data.value ) me.setData({value:me.data.defvalue});
    },
    onChange:function (e) {
        var me = this;
        if( me.data.value !== me.data.valuetmp ){
            me.setData({
                value:me.data.valuetmp || me.data.defvalue,
                inedit:false
            });
        }

    },
    onEdit:function (e) {
        var me = this;

        me.setData({
            valuetmp:me.data.value,
            inedit:true
        });
    },
    onHide:function (e) {
        var me = this;

        if( me.data.inedit ){
            me.setData({
                inedit:false
            })
        }

    },
    onKeypress:function (e) {
        var me = this;

        if( $.toInt(e.keyCode) === 13 ){
            me.onChange(e);
        }
    }
});