<div>
    <div class="width-full inplace-edit" u-outside-click="onHide">
        <div class="value ${inedit ? 'hidden':''}">
            <a u-v="value" u-click="onEdit"></a>
        </div>
        <div class="clearfix ${inedit ? '' : 'hidden'}">
            <div class="pull-right btntool">
                <button class="button is-primary btn-xs" u-click="onChange">确定</button>
                <button class="button btn-xs" u-click="onHide">取消</button>
            </div>
            <div class="content">
                <textarea type="text" rows="6"
                          class="form-control"
                          u-v="valuetmp"></textarea>
            </div>
        </div>
    </div>
</div>