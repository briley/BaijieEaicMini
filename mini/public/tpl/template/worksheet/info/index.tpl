<page>
    <div class="hp-n vm-n is-size-6 has-text-weight-semibold">
        工单信息
    </div>
    <div class="list-group">
        <div class="list-item">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    工单号
                </div>
                <div class="">
                    ${sheet.sheetid}
                </div>
            </div>
        </div>
        <div class="list-item" >
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    状态
                </div>
                <div class="lm-m flex-middle has-text-danger">
                    ${status_str[sheet.status]}
                </div>
            </div>
        </div>

        <div class="list-item">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    发起时间
                </div>
                <div>
                    ${sheet.createtime}
                </div>
            </div>
        </div>

        <div class="list-item" u-if="sheet.status==2 || sheet.status==3">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    接单人
                </div>
                <div class="lm-m flex-middle has-text-warning">
                    ${sheet.userto_user.name}

                </div>
            </div>
        </div>
        <div class="list-item" u-if="sheet.status==2 || sheet.status==3">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    接单时间
                </div>
                <div class="lm-m flex-middle has-text-warning">
                    ${sheet.totime}

                </div>
            </div>
        </div>
    </div>

    <div class="hp-n vm-n is-size-6 has-text-weight-semibold">
        发起人
    </div>
    <div class="list-group">

        <div class="list-item">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    姓名
                </div>
                <div class="lm-m flex-middle">
                    <div class="flex-middle">
                        <div class="image is-16x16 circle-border">
                            <img src="${sheet.usericon}" class="cover-image"/>
                        </div>
                    </div>
                    <div>${sheet.username}</div>

                </div>
            </div>
        </div>
        <div class="list-item">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    联系电话
                </div>
                <div>
                    <a href="tel:${sheet.mobile}">${sheet.mobile}</a>
                </div>
            </div>
        </div>
        <div u-if="{|sheet.userbed && sheet.userbed.plot |}" class="list-item">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    小区
                </div>
                <div>
                    ${sheet.userbed.plot.name}
                </div>
            </div>
        </div>
        <div u-if="{|sheet.userbed && sheet.userbed.dorm |}" class="list-item">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    宿舍
                </div>
                <div>
                    ${sheet.userbed.dorm.name}
                </div>
            </div>
        </div>
        <div u-if="{|sheet.userbed && sheet.userbed.room |}" class="list-item">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    房间
                </div>
                <div>
                    ${sheet.userbed.room.name}
                </div>
            </div>
        </div>
    </div>

    <div class="hp-n vm-n is-size-6 has-text-weight-semibold">
        定位
    </div>
    <div class="list-group">
        <div class="list-item">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    小区
                </div>
                <div>
                    ${sheet.plotname}
                </div>
            </div>
        </div>
        <div class="list-item" u-if="$.toInt(sheet.dormid)">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    宿舍
                </div>
                <div class="lm-m flex-middle">
                    ${sheet.dormname}

                </div>
            </div>
        </div>
        <div class="list-item" u-if="$.toInt(sheet.roomid)">
            <div class="flex-middle">
                <div class="has-text-info width-60">
                    房间
                </div>
                <div>
                    ${sheet.roomname}
                </div>
            </div>
        </div>

    </div>

    <div class="hp-n vm-n is-size-6 has-text-weight-semibold">
        内容
    </div>
    <div class="list-group">

        <div class="p-n">
            ${sheet.content}
        </div>
    </div>

    <div u-if="{|sheet.status==3||sheet.status==5|}">
        <div class="hp-n vm-n is-size-6 has-text-weight-semibold">
            回复
        </div>
        <div class="list-group">

            <div class="p-n has-background-white-ter has-text-danger">
                ${sheet.reply}
            </div>
        </div>

        <div class="hp-n vm-n is-size-6 has-text-weight-semibold">
            完成状态
        </div>
        <div class="list-group">

            <div class="p-n has-background-white-ter has-text-danger">
                ${finishstatus_str[sheet.finishstatus]}
            </div>
        </div>


    </div>


</page>