<div>
    <div class="p-n has-background-white">
        <nav class="pagination" role="navigation" aria-label="pagination">
            <a class="pagination-previous" u-click="onPrevPage">←上一页</a>
            <a class="pagination-next" u-click="onNextPage">下一页→</a>
            <ul class="pagination-list is-hidden-mobile">
                <li u-each="pagerange(pageindex,pagecount)"
                    u-item="item"
                    title="item" class='btn btn-${item==pageindex ? "primary":"default"}'
                    u-click="go_page" pageindex="${item}">
                    <a class="pagination-link" aria-label="${item}">${item}</a>
                </li>

            </ul>
        </nav>
    </div>


</div>