/*========== 系统配置，请根据具体项目修改 [begin] ======================*/
module.exports = {

	//小程序的名称
	appName: "您的小程序名称",

	//后台服务器地址
	server_url: "https://ailixin-mini.baijienet.com",

	//图片根地址
	image_root: "https://ailixin-mini.baijienet.com/images/mall",

	//用于读取当前登录用户信息的api
	apiLoadCur: "mall.User.LoadCur",


	//实例ID，非云端环境时填 0 即可
	//tenantid: 0,

	//用于登录的页面
	loginPage: "/pages/person/login",

	//用于检查后台session有效期的api
	//apiCheckSession: "site.Sys.CheckSession",

	//用于session续期的api
	//apiResumeSession: "site.Sys.ResumeSession",

	//用于登录的api
	//'apiMiniLogin' : 'site.Sys.MiniLogin',

	//bizcall地址(与后台保持一致，一般不用修改此项)
	//bizroot: "/bizservice"
};

/*========== 系统配置，请根据具体项目修改 [end] ======================*/

