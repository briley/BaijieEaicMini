var $ = getApp().globalData.eui;
var Decimal = require("../../decimal.js");
$.page({
    data: {
        "addrid": 0,
        "recvaddr": {},
        "product": {},
        "sku": {},
        "productid": 0,
        "skuid": 0,
        "num": 1,
        "icon": "",
        "price": "",
        fromuser:0,
        curUser: null, //当前登录用户信息
    },
    onLoad: function (options) {

        var me = this;

        $.log("====>buynow options:" + $.toJson(options));

        var num = $.toInt(options.num);
        if (num < 1) num = 1;

        var skuid = $.toInt(options.skuid);

        me.setData({
            productid: $.toInt(options.id),
            skuid: skuid,
            num: num,
            fromuser:$.toInt(options.fromuser)
        });

        var fnLoad = function () {
            $.api("mall.Product.Load", {
                    id: me.data.productid
                })
                .then(function (product) {
                    me.setData({
                        product: product,
                        //icon: product.icon,
                        //price: product.saleprice
                    });
                    var isvip = 0;
                    if (me.data.fromuser ) {
                        isvip = 1;
                    }
                    if (skuid) {
                        $.api("mall.Product.LoadSku", {
                                id: me.data.productid,
                                skuid: skuid
                            })
                            .then(function (sku) {
                                var icon = sku.icon;
                                if (icon.indexOf("/") === 0) icon = $.getConfig("server_url") + icon;
                                me.setData({
                                    sku: sku,
                                    icon: icon,
                                    price: new Decimal($.toFloat(isvip ? sku.vipprice : sku.saleprice)).toNumber()
                                });
                            });
                    }
                    $.api("mall.RecvAddress.LoadDefault", {}).then(function (value) {
                        if (value && $.isObject(value)) {
                            me.setData({
                                "addrid": value.id,
                                "recvaddr": value
                            });

                        }
                    });
                });
        };

        $.isLogined()
            .then(function (bLogined) {
                if (bLogined) {
                    $.api($.getConfig("apiLoadCur"), {})
                        .then(function (value) {
                            me.setData({
                                "curUser": value
                            });
                            fnLoad();
                        });
                } else {
                    fnLoad();
                }
            }, function (err) {
                $.errorBox("错误", err.message)
            });

    },
    onClickSelAddr: function (e) {

        var me = this;

        $.go("/pages/person/recvaddress/selrecvaddr");

    },
    onclick47: function (e) {

        var me = this;

        if (me.data.num > 1) me.setData({
            num: me.data.num - 1
        })

    },
    onclick51: function (e) {

        var me = this;

        me.setData({
            num: me.data.num + 1
        })

    },
    onclick61: function (e) {

        var me = this;

        if (!me.data.addrid) {
            $.errorBox("错误", "请选择收货地址", function () {});
            return;
        }

        var products = [];
        products.push({
            id: me.data.productid,
            skuid: me.data.skuid,
            num: me.data.num,
            fromuser:me.data.fromuser
        });


        $.api("mall.Order.Add", {
                products: $.toJson(products),
                addrid: me.data.addrid
            })
            .then(function (value) {
                //$.alertBox("提示","提交成功");
                $.api("mall.Order.Pay", {
                        orderid: value
                    })
                    .then(function (value) {
                        wx.requestPayment({
                            'timeStamp': value.timeStamp,
                            'nonceStr': value.nonceStr,
                            'package': value.package,
                            'signType': 'MD5',
                            'paySign': value.paySign,
                            'success': function (res) {
                                $.alertBox("提示", "支付成功")
                                    .then(function () {
                                        $.back();
                                    })
                            },
                            'fail': function (res) {
                                console.log("支付 res:" + $.toJson(res));
                                //支付 res:{"err_code":"-1","err_desc":"调用支付JSAPI缺少参数: package","errMsg":"requestPayment:fail"}
                                if (res.errMsg.indexOf("fail cancel")) {
                                    $.alertBox("提示", "支付取消");
                                } else {
                                    $.errorBox("错误", "支付失败：" + res.errMsg + " 原因：" + res.err_desc);
                                }

                            },
                            'complete': function (res) {
                                //$.back();
                            }
                        })
                    });
            })

    },
    notifySelRecvAddress: function (addr) {

        var me = this;
        me.setData({
            addrid: addr.id,
            recvaddr: addr
        });
    }
});