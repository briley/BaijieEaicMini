var $ = getApp().globalData.eui;

//本组件实现倒计时显示
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        expireTime:{
            type:String,
            value:""
        },
        display:{
            type:String,
            value:"normal"//normal普通 emphasize强调
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        "iDay" : 0,
        "iHour" : 0,
        "iMinute" : 0,
        "iSecond" : 0,
        isend:true,
        intval:0
    },
    options: {
        addGlobalClass: true,
    },
    observers:{
        "expireTime" : function (et) {
            var me = this;
            if( me.data.intval ) clearInterval(me.data.intval);
            me.data.intval = setInterval(function () {
                var tNow = new Date();
                var tEnd = $.convertDateFromString(et);
                if( tEnd <= tNow ){
                    me.setData({
                        "isend" : true
                    })
                }else{
                    var diff = tEnd - tNow;
                    var minute = 1000 * 60;      //把分，时，天，周，半个月，一个月用毫秒表示
                    var hour = minute * 60;
                    var day = hour * 24;
                    var iDay = $.toInt(diff / day);
                    var iHour = $.toInt( (diff - iDay * day)/hour );
                    var iMinute = $.toInt( (diff - iDay * day - iHour * hour)/minute);
                    var iSecond = $.toInt( (diff - iDay * day - iHour * hour-iMinute*minute)/1000);
                    me.setData({
                        "iDay" : iDay,
                        "iHour" : iHour,
                        "iMinute" : iMinute,
                        "iSecond" : iSecond,
                        "isend" : false
                    });
                }
            },1000);
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {},

    lifetimes: {
        // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
        attached: function () {
            var me = this;

        },
        detached: function () {
            var me = this;
            if( me.intval ){
                clearInterval(me.intval);
            }
        },
    },
})
