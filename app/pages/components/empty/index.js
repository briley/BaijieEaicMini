// pages/components/empty/index.js
var $ = getApp().globalData.eui;
$.component({
    /**
     * 组件的属性列表
     */
    properties: {
        title: {
            type: String,
            value:"没有数据"
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})