var $ = getApp().globalData.eui;

Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		loginUser:{
			type:Object,
			value:null
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {

		"meta": {
			"datas": [],
			"cond": {
				"pageindex": 1,
				"pagesize": 10
			}
		},
		zibo: {
			datas: []
		},
		"slide_2": {
			"datas": []
		},
		new_products:[],
		topbanner:$.getConfig("image_root")+"/topbanner.png",

	},
	options: {
		addGlobalClass: true,
	},
	/**
	 * 组件的方法列表
	 */
	methods: {
		refresh: function () {

			var me = this;
			me.data.meta.cond.pageindex = 0;
			me.loadNextPage();


		},

		loadNextPage: function () {

			var me = this;
			var cond = me.data.meta.cond;
			cond.pageindex++;
			if (cond.pageindex === 1) me.data.meta.datas = []; //第一页要重置
			$.api("mall.Product.ListPage", cond)
				.then(function (value) {
					var datas = me.data.meta.datas;
					if ($.count(value.datas) > 0) datas = datas.concat(value.datas);
					me.setData({
						"meta.datas": datas
					});

				}, function (reason) {
					console.log(reason.message);
				})

		},
		onViewProduct: function (e) {

			var me = this;

			$.go("/pages/product/detail?id=" + e.currentTarget.dataset.id);
			//$.go("/pages/product/detail?scene=dist:" + e.currentTarget.dataset.id+"_1");

		},
		onScrollViewUpload1: function (e) {

			var me = this;

			me.loadNextPage();

		},
		onScrollViewDownRefresh1: function (e) {

			var me = this;

			me.refresh();

		},
		onViewNewProduct:function (e) {
			$.go("/pages/product/catlist?catid=-1");
		},
		onSearch:function (e) {
			$.go("/pages/product/search/index")
		}
	},
	lifetimes: {
		// 生命周期函数，可以为函数，或一个在methods段中定义的方法名
		attached: function () {
			var me = this;

			me.refresh();

			$.api("live.Room.ListPage",{"pagesize":10,"pageindex":1})
				.then(function (value) {
					$.each(value.datas,function (v) {
						v.status = $.toInt(v.status);
						if( !v.anchoricon){
							v.anchoricon = "/images/userw.png";
						}else{
							v.anchoricon = $.getConfig("image_root")+v.anchoricon;
						}
						$.log("--- starttime:"+parseInt(v.starttime));
						var start = new Date($.toInt(v.starttime)*1000);
						v.starttime = ""+(start.getMonth()+1)+"月"+start.getDate()+"日 "+(start.getHours()<10?'0':'')+start.getHours()+":"+(start.getMinutes()<10?'0':'')+start.getMinutes();
						livePlayer.getLiveStatus({ room_id: $.toInt(v.roomid) })
							.then(res => {
								// 101: 直播中, 102: 未开始, 103: 已结束, 104: 禁播, 105: 暂停中, 106: 异常，107：已过期
								$.log("-------->"+$.toJson(res));
								const liveStatus = res.liveStatus
								console.log('get live status', liveStatus)
							})
							.catch(err => {
								console.log('get live status', err)
							})
					});
					me.setData({"zibo":value});
				});

			$.api("mall.Slide.LoadAllByCat", {
				cat: "home"
			}).then(function (data) {
				$.each(data, function (d) {
					var url = d.image;
					if (url.indexOf("http") !== 0) {
						url = $.getConfig("server_url") + url;
						d.image = url;
					}
				});
				me.setData({
					"slide_2.datas": data
				});

			}, function (err) {
				$.errorBox("错误", err.message)
			});
			$.api("mall.Product.ListPage",{
				pagesize:3,
				pageindex:1,
				isnew:1
			})
				.then(function (value) {
					me.setData({"new_products":value.datas});
				})

		},
		detached: function () {
			var me = this;

		},
	},
});
