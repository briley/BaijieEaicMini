//本组件用于展示富文本编辑器的编辑的内容
var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        entities:{
            type:Array,
            value:null
        }
    },

    /**
     * 组件的初始数据
     */
    data: {},

    /**
     * 组件的方法列表
     */
    methods: {},
    lifetimes:{
        attached:function () {
            var me = this;
            //$.log("====>entities=" + $.toJson(me.data.entities));
        }
    }
});
