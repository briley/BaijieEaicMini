// pages/components/image.js
var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        entity: {
            type: Object,
            value: null
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        mode:"widthFix"
    },

    /**
     * 组件的方法列表
     */
    methods: {},
    lifetimes: {
        attached: function() {
            var me = this;
            var entity = me.data.entity;
            // 在组件实例进入页面节点树时执行
            var url = entity.value.url;

            if( url.indexOf("/")===0){
                entity.value.url = $.getConfig("server_url")+url;
                me.setData({"entity":entity});
            }
            if( entity.value.size.height.type == 4){//高度指定，则用cover-image模式
                me.setData({"mode":""});
            }
        },
        detached: function() {
            // 在组件实例被从页面节点树移除时执行
        },
    }
});
