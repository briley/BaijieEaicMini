var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        home_news:{}
    },
    options: {
        addGlobalClass: true,
    },
    /**
     * 组件的方法列表
     */
    methods: {
        onView:function (e) {
            var me = this;
            var id = $.toInt(e.currentTarget.dataset.id);
            if( id ){
                $.go("/pages/cms/news/detail?id="+id);
            }
        }
    },
    lifetimes: {
        // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
        attached: function () {
            var me = this;
            $.api("cms.News.LoadHome",{})
                .then(function (value) {
                    value.lasttime = $.timeago(value.lasttime);
                    me.setData({"home_news":value});
                })
        },
        detached: function () {
            var me = this;
            if (me.intval) {
                clearInterval(me.intval);
            }
        },
    },
})