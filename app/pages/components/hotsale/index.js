var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        "meta": {
            "datas": [],
            "cond": {
                "pageindex": 1,
                "pagesize": 3
            }
        },
    },
    options: {
        addGlobalClass: true,
    },
    /**
     * 组件的方法列表
     */
    methods: {
        refresh: function () {

            var me = this;
            me.data.meta.cond.pageindex = 0;
            me.loadNextPage();
        },
    
        loadNextPage: function () {
    
            var me = this;
            var cond = me.data.meta.cond;
            cond.pageindex++;
            if (cond.pageindex === 1) me.data.meta.datas = []; //第一页要重置
            $.api("mall.Product.ListPage", cond)
                .then(function (value) {
                    var datas = me.data.meta.datas;
                    if ($.count(value.datas) > 0) datas = datas.concat(value.datas);
                    me.setData({
                        "meta.datas": datas,
                        "meta.cond.pageindex":cond.pageindex
                    });
    
                }, function (reason) {
                    console.log(reason.message);
                })
    
        },
        onViewProduct: function (e) {

            var me = this;
    
            $.go("/pages/product/detail?id=" + e.currentTarget.dataset.id);
            //$.go("/pages/product/detail?scene=dist:" + e.currentTarget.dataset.id+"_1");
    
        },
    },
    lifetimes: {
        // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
        attached: function () {
            var me = this;
            me.refresh();
        },
        detached: function () {
            var me = this;
            if (me.intval) {
                clearInterval(me.intval);
            }
        },
    },
})