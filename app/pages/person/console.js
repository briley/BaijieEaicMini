var $ = getApp().globalData.eui;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        "weburl":""
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var me = this;
        $.getSessionKey()
            .then(function (sid) {
                var weburl=$.getConfig("server_url")+"/console/"+$.getConfig("tenantid")+"?sid="+sid;
                $.log("后台url:"+weburl);
                me.setData({
                    "weburl":weburl
                });
            })

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    bindmessage1:function(e){

        var me = this;

        //$.log("收到消息:"+$.toJson(e.detail));
        //$.notify("HaibaoMessage",[e.detail]);
        //$.go("/pages/templ/save?url="+e.detail.data[0].url);

    }
})