// pages/person/distcenter/apply/index.js
var $ = getApp().globalData.eui;

$.page({

	/**
	 * 页面的初始数据
	 */
	data: {
		applys: [],
		reapply: false,
		status_arr: ["审核中", "已通过", "已拒绝"],
		meta: {
			invitesn: "",
			name: "",
			mobile: "",
			province:"广东省",
			city:"广州市",
			region:"天河区",
			weixin:"",
			subscribe:0
		},
		region: ['广东省', '广州市', '天河区'],
		msgid: ""//订阅消息的模板id
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var me = this;
		$.api("mall.DistUserApply.LoadAll", {})
			.then(function (value) {
				$.each(value, function (v) {
					v.status = $.toInt(v.status);
				});
				me.setData({
					"applys": value
				});
				return $.api("site.Subscribe.LoadMsgID", {"type": "applyresult"})
			})
			.then(function (value) {
				me.setData({
					"msgid": value
				})
			})
	},
	onSubmit: function (e) {
		var me = this;
		var meta = me.data.meta;
		if (!meta.name) {
			$.errorBox("错误", "请填写姓名");
			return;
		}
		if (!meta.mobile) {
			$.errorBox("错误", "请填写手机号");
			return;
		}
		if (!meta.weixin) {
			$.errorBox("错误", "请填写微信号");
			return;
		}
		var fnSubmit=function(meta){
			$.api("mall.DistUserApply.Add", meta)
				.then(function (value) {
					$.alertBox("提示", "提交成功，请等待审核")
						.then(function (value) {
							$.back();
						})
				}, function (err) {
					$.errorBox("错误", err.message);
				})
		};
		wx.requestSubscribeMessage({
			tmplIds: [me.data.msgid],
			success: function (res) {
				var isOK=false;
				if( res.errMsg==="requestSubscribeMessage:ok" ){
					$.each(res.result,function (r) {
						if( r[me.data.msgid] === "accept"){
							isOK = true;
							return true;
						}
					})
				}
				if( isOK ){
					me.data.meta.subscribe=1;
				}
				fnSubmit(me.data.meta);
			},
			fail(err) {
				$.errorBox("错误", "订阅消息失败："+err.errMsg);
			}
		})

	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	},
	onReapply: function (e) {
		this.setData({
			"reapply": true
		});
	},
	bindRegionChange: function (e) {
		//console.log('picker发送选择改变，携带值为', e.detail.value)
		var arr=e.detail.value;
		var province="";
		var city="";
		var region="";
		if( arr.length ) province=arr[0];
		if( arr.length>1) city=arr[1];
		if( arr.length>2) region=arr[2];
		this.setData({
			region: e.detail.value,
			"meta.province":province,
			"meta.city":city,
			"meta.region":region
		})
	}
})