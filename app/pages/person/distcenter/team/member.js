var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        level: {
            type: Number,
            value: 1
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        meta: {
            cond: {
                pageindex: 1,
                pagesize: 8,
                level: 1
            },
            datas: [],
            totalpage: 0,
            totalcount: 0
        }
    },

    options: {
        addGlobalClass: true,
    },
    /**
     * 组件的方法列表
     */
    methods: {
        onScrollViewUpload: function (e) {

            var me = this;

            me.loadNextPage();

        },
        onScrollViewDownRefresh: function (e) {

            var me = this;

            me.refresh();

        },
        refresh: function () {

            var me = this;
            me.data.meta.cond.pageindex = 0;
            me.loadNextPage();
        },
        loadNextPage: function () {

            var me = this;
            var cond = me.data.meta.cond;
            cond.level = me.data.level;


            cond.pageindex++;
            if (cond.pageindex === 1) me.data.meta.datas = []; //第一页要重置

            $.api("mall.User.ListPageLevel", cond)
                .then(function (value) {
                    var p = me.data.meta.datas;


                    if ($.count(value.datas) > 0)
                        p = p.concat(value.datas);
                    me.setData({
                        "meta.datas": p,
                        "meta.totalpage": value.totalpage,
                        "meta.totalcount": value.totalcount
                    });

                }, function (reason) {
                    console.log(reason.message);
                })

        }
    },
    lifetimes: {
        // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
        attached: function () {
            var me = this;

            me.refresh();
        },
        detached: function () {
            var me = this;

        },
    },
})