var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        team_totalcount: 0,
    },
    options: {
        addGlobalClass: true,
    },

    /**
     * 组件的方法列表
     */
    methods: {

    },
    lifetimes: {
        // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
        attached: function () {
            var me = this;

            $.api("mall.User.GetTeamTotalCount", {})
                .then(function (value) {
                    me.setData({
                        "team_totalcount": $.toInt(value)
                    });

                })
        },
        detached: function () {
            var me = this;

        },
    },
})