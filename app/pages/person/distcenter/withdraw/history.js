// pages/person/distcenter/withdraw/history.js
var $ = getApp().globalData.eui;
$.page({

    /**
     * 页面的初始数据
     */
    data: {
        total: 0,
        meta: {
            datas: [],
            totalpage: 0,
            totalcount: 0,
            cond: {
                pageindex: 1,
                pagesize: 8
            }
        },
        status_text_arr: ["已申请", "待打款", "审核不通过", "已打款"],
        status_color_arr: ["has-text-primary", "has-text-warning", "has-text-danger", "has-text-success"],
        check_step_status_arr: ["is-normal", "is-finish", "is-fail", "is-finish"]
        
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var me = this;
        $.api("mall.Withdraw.GetTotal", {})
            .then(function (value) {
                me.setData({
                    "total": $.toFloat(value).toFixed(2)
                })
            });
        me.refresh();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    onScrollViewUpload: function (e) {

        var me = this;

        me.loadNextPage();

    },
    onScrollViewDownRefresh: function (e) {

        var me = this;

        me.refresh();

    },
    refresh: function () {

        var me = this;
        me.data.meta.cond.pageindex = 0;
        me.loadNextPage();
    },
    loadNextPage: function () {

        var me = this;
        var cond = me.data.meta.cond;

        cond.pageindex++;
        if (cond.pageindex === 1) me.data.meta.datas = []; //第一页要重置

        $.api("mall.Withdraw.ListPage", cond)
            .then(function (value) {
                $.each(value.datas, function (d) {
                    d.status = $.toInt(d.status);
                    d.type = $.toInt(d.type);
                    if (d.type === 0) d.typename = "余额";
                    else if (d.type == 1) d.typename = "微信钱包";
                    else if (d.type == 2) d.typename = "" + d.bankname + " [" + d.cardno + "] " + d.name;
                    else if (d.type === 3) d.typename = "支付宝：" + d.zhihubao;
                });
                var p = me.data.meta.datas;

                if ($.count(value.datas) > 0)
                    p = p.concat(value.datas);
                me.setData({
                    "meta.datas": p,
                    "meta.totalpage": value.totalpage,
                    "meta.totalcount": value.totalcount
                });

            }, function (reason) {
                console.log(reason.message);
            })

    },
    onToggleItem: function (e) {
        var me = this;
        var index = $.toInt(e.currentTarget.dataset.index);
        var item = me.data.meta.datas[index];
        item.expand = !item.expand;
        me.setData({ "meta.datas": me.data.meta.datas });
    }
})