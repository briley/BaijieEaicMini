var $ = getApp().globalData.eui;
$.page({

    /**
     * 页面的初始数据
     */
    data: {
        money: 0,
        type: 0,
        cardindex: 0,
        zhihubao: "",
        valid_kickback: 0, //可提现金额
        withtype: ["提现到余额", "提现到微信钱包", "提现到银行卡", "提到到支付宝"],
        cards: [],
        card_for_sel: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var me = this;
        $.api("mall.WithdrawCard.LoadAll", {})
            .then(function (value) {
                var arr = [];
                $.each(value, function (v) {
                    arr.push(v.bankname + " [" + v.cardno + "] " + v.name);
                });
                me.setData({
                    "cards": value,
                    "card_for_sel": arr
                });
                return $.api("mall.User.GetValidKickback", {})
            })
            .then(function (value) {
                me.setData({
                    "valid_kickback": $.toFloat(value).toFixed(2)
                });

            })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    bindPickerChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            type: $.toInt(e.detail.value)
        })
    },
    bindPickerChangeCard: function (e) {
        var me = this;
        me.setData({
            cardindex: $.toInt(e.detail.value)
        })
    },
    onSubmit: function (e) {
        var me = this;
        var money = $.toFloat(me.data.money);
        if (money <= 0 || money > me.data.valid_kickback) {
            $.errorBox("错误", "请输入合理的取现金额");
        } else {
            var type = $.toInt(me.data.type);
            if (type === 3 && !me.data.zhihubao) {
                $.errorBox("错误", "请输入支付宝帐号");
            } else {
                var meta = {
                    money: money,
                    type: type
                };
                if (type === 3) {
                    meta.zhihubao = me.data.zhihubao;
                } else if (type === 2) {
                    var index = $.toInt(me.data.cardindex);
                    if (index >= 0 && index < me.data.cards.length) {
                        meta.cardid = me.data.cards[index].id;

                    } else {
                        $.errorBox("错误", "请选择银行卡");
                    }
                }

                $.api("mall.Withdraw.Apply", meta)
                    .then(function (value) {
                        $.alertBox("提示", "已成功发起提现申请，预计3－7个工作日发放至个人用户", function () {
                            $.back();
                        });
                    }, function (err) {
                        $.errorBox("错误", err.message);
                    })
            }
        }
    }
})