var $ = getApp().globalData.eui;
$.page({

    /**
     * 页面的初始数据
     */
    data: {
        team_totalcount: 0,
        fans_totalcount:0,
        customer_totalcount:0,
        total_kickback: 0, //所有的佣金
        valid_kickback: 0, //可提现金额
        total_revenue: 0, //总的营业额
        today_income:0,//今日收入
        today_order:0,//今日成交量
        today_fans:0,//今日拉粉数
        today_customer:0//今日新增客户
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var me = this;
        $.isLogined()
            .then(function (bLogined) {
                if (!bLogined) {
                    wx.redirectTo({
                        url: '/pages/person/login',
                    });
                    return;
                }
                $.api($.getConfig("apiLoadCur"), {})
                    .then(function (oUserInfo) {
                        oUserInfo.isdist = $.toInt(oUserInfo.isdist);
                        if (!oUserInfo.isdist) {
                            wx.redirectTo({
                                url: '/pages/person/distcenter/apply/index',
                            });
                            return;
                        }
                        $.api("mall.User.GetTeamTotalCount", {})
                            .then(function (value) {
                                me.setData({
                                    "team_totalcount": $.toInt(value)
                                });
                                return $.api("mall.User.GetTotalKickback", {})
                            })
                            .then(function (value) {
                                me.setData({
                                    "total_kickback": $.toFloat(value).toFixed(2)
                                });
                                return $.api("mall.User.GetValidKickback", {})

                            })
                            .then(function (value) {
                                me.setData({
                                    "valid_kickback": value
                                });
                                return $.api("mall.User.GetTotalRevenue", {})
                            })
                            .then(function (value) {
                                me.setData({
                                    "total_revenue": value
                                });
                                return $.api("mall.User.GetTodayIncome")
                            })
                            .then(function (value) {
                                me.setData({"today_income":value});
                                return $.api("mall.User.GetTodayOrder")
                            })
                            .then(function (value) {
                                me.setData({"today_order":value});
                                return $.api("mall.User.GetTodayFans")
                            })
                            .then(function (value) {
                                me.setData({"today_fans":value});
                                return $.api("mall.User.GetTodayCustomer")
                            })
                            .then(function (value) {
                                me.setData({"today_customer":value});
                                return $.api("mall.User.GetTotalFans",{})
                            })
                            .then(function (value) {
                                me.setData({"fans_totalcount":value});
                                return $.api("mall.User.GetTotalCustomer",{})
                            })
                            .then(function(value){
                                me.setData({"customer_totalcount":value})
                            })
                    })

            })

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    onViewRule: function (e) {
        $.go("/pages/person/distcenter/withdraw/rule");
    },
    onWithDraw: function (e) {
        $.go("/pages/person/distcenter/withdraw/index");
    },
    onViewCard: function (e) {
        $.go("/pages/person/distcenter/card/index");
    },
    onViewTeam: function (e) {
        $.go("/pages/person/distcenter/team/index");
    },
    onViewFans:function(e){
        $.go("/pages/person/distcenter/fans/index");
    },
    onViewCustomer:function(e){
        $.go("/pages/person/distcenter/customer/index");
    },
    onViewIncome: function (e) {
        $.go("/pages/person/distcenter/income/index")
    },
    onViewWithdrawHis: function (e) {
        $.go("/pages/person/distcenter/withdraw/history")
    },
    onViewHaibao: function (e) {
        $.go("/pages/person/haibao/seltempl");
    }
})